package com.company;

import com.company.challenge1.GuiControllerChallenge1;
import com.company.challenge2.GuiControllerChallenge2;

public class Main {

    public static void main(String[] args) {
        GuiControllerChallenge1 gui1 = new GuiControllerChallenge1();
        GuiControllerChallenge2 gui2 = new GuiControllerChallenge2();

        gui1.launchGui();
        //gui2.launchGui();

    }
}
