package com.company.challenge1;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;

public class GuiControllerChallenge1 {

    private final JTextField accountNameTextField;
    private final JTextField mailTextField;
    private final JTextField websiteTextField;

    private final JLabel imageLabel;

    private final JPanel generalPanel;
    private final JButton startButton;

    private final JFrame mainFrame;

    public GuiControllerChallenge1(){
        mainFrame = new JFrame();
        mainFrame.setTitle("Challenge1 Gorini Francesco");
        mainFrame.setResizable(false);

        imageLabel = new JLabel();

        accountNameTextField = new JTextField("Account-name");
        accountNameTextField.setPreferredSize(new Dimension(300,20));

        mailTextField = new JTextField("Mail");
        mailTextField.setPreferredSize(new Dimension(300, 20));

        websiteTextField = new JTextField("Website");
        websiteTextField.setPreferredSize(new Dimension(300,20));

        startButton = new JButton("Submit");

        startButton.addActionListener(e ->{
            SwingUtilities.invokeLater(()->{
                generateQR(accountNameTextField.getText(), mailTextField.getText(), websiteTextField.getText());
            });
        });

        generalPanel = new JPanel();
        generalPanel.setBorder(new EmptyBorder(10,10,10,10));
        generalPanel.setSize(new Dimension(1000, 1000));

        generalPanel.add(accountNameTextField);
        generalPanel.add(mailTextField);
        generalPanel.add(websiteTextField);
        generalPanel.add(startButton);
        generalPanel.add(imageLabel);

        mainFrame.setContentPane(generalPanel);
        mainFrame.setSize(new Dimension(1000, 500));
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent ev) {
                System.exit(-1);
            }
            public void windowClosed(final WindowEvent ev) {
                System.exit(-1);
            }
        });
    }

    private void generateQR(String accountName, String mail, String website) {
        try {
            System.out.println("enter");
            Image image = null;
            URL url = new URL("https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" + accountName + mail + website);
            image = ImageIO.read(url);

            imageLabel.setIcon(new javax.swing.ImageIcon(image));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void launchGui() {
        mainFrame.setVisible(true);
    }
}
