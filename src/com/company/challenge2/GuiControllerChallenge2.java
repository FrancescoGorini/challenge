package com.company.challenge2;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.LinkedList;

public class GuiControllerChallenge2 {


    private final JPanel generalPanel;
    private final JButton startButton;

    private final JLabel imageLabel;

    private final JFrame mainFrame;

    private final JComboBox<String> friendsComboBox;

    public GuiControllerChallenge2() {
        mainFrame = new JFrame();
        mainFrame.setTitle("Challenge2 Gorini Francesco");
        mainFrame.setResizable(false);

        imageLabel = new JLabel();

        AccountFriend aurora = new AccountFriend("aurora", "www", "a@i.com");
        AccountFriend riccardo = new AccountFriend("riccardo", "www", "a@i.com");
        AccountFriend luana = new AccountFriend("luana", "www", "a@i.com");
        LinkedList<AccountFriend> friends = new LinkedList<>();
        friends.add(aurora);
        friends.add(riccardo);
        friends.add(luana);

        final String[] friendsValues = new String[] { aurora.getName(), riccardo.getName(), luana.getName() };
        friendsComboBox = new JComboBox<>(friendsValues);

        startButton = new JButton("Submit");
        startButton.addActionListener(e ->{
            SwingUtilities.invokeLater(()->{
                generateFriendQR(friends.get(friendsComboBox.getSelectedIndex()));
            });
        });

        generalPanel = new JPanel();
        generalPanel.setBorder(new EmptyBorder(10,10,10,10));
        generalPanel.setSize(new Dimension(1000, 1000));

        generalPanel.add(friendsComboBox);

        generalPanel.add(startButton);
        generalPanel.add(imageLabel);

        mainFrame.setContentPane(generalPanel);
        mainFrame.setSize(new Dimension(1000, 500));
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent ev) {
                System.exit(-1);
            }
            public void windowClosed(final WindowEvent ev) {
                System.exit(-1);
            }
        });
    }

    public void launchGui() {
        mainFrame.setVisible(true);
    }

    private void generateFriendQR(AccountFriend friend) {
        try {
            Image image = null;
            System.out.println(friend.getName());
            URL url = new URL("https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" + friend.getName()
                    + friend.getMail() + friend.getWebsite());
            image = ImageIO.read(url);

            imageLabel.setIcon(new javax.swing.ImageIcon(image));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
