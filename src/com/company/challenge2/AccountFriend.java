package com.company.challenge2;

public class AccountFriend {
    private String name;
    private String website;
    private String mail;

    public AccountFriend(String name, String website, String mail){
        this.name = name;
        this.website = website;
        this.mail = mail;
    }

    public String getName(){
        return this.name;
    }

    public String getWebsite(){
        return this.website;
    }

    public String getMail(){
        return this.mail;
    }
}
